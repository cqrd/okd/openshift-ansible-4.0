# README

- [OpenShift 4: Install Experience](https://blog.openshift.com/openshift-4-install-experience/)

## CentOS/RHEL (虚拟机)配置

```bash
# 基于桥接模式设置固定 ip
cat >> /etc/sysconfig/network-scripts/ifcfg-enp0s3 << EOF
# network setting
BOOTPROTO=static
IPADDR=192.168.1.84
GATEWAY=192.168.1.1
DNS1=8.8.8.8
DNS2=114.114.114.114
EOF
service network restart

# 切换默认的 yum 源
ansible oso -u root -i hosts -m shell -a "mv /etc/yum.repos.d/CentOS-Base.repo /etc/yum.repos.d/CentOS-Base.repo.bak"
ansible oso -u root -i hosts -m shell -a "curl -o /etc/yum.repos.d/CentOS-Base.repo http://mirrors.aliyun.com/repo/Centos-7.repo"
ansible oso -u root -i hosts -m shell -a "yum update -y"
```

## Origin 部署及卸载

```bash
# 获取 ansible 脚本
git clone --depth=1 -b release-4.0 https://github.com/openshift/openshift-ansible openshift-ansible-4.0

# 定义个性化配置
mkdir -p _ops

# 更新 yum
ansible oso -i hosts -m shell -a "yum update -y"

# RPM-based installation
## 本地虚拟机
ansible-playbook -i _ops/origin/inventory/okd.hosts -vvv _ops/origin/uninstall.yml
ansible-playbook -i _ops/origin/inventory/okd.hosts -vvv _ops/origin/install.yml -e env=home

## 重庆服务器
ansible-playbook -i _ops/origin/inventory/cqrd/okd.hosts -vvv _ops/origin/uninstall.yml
ansible-playbook -i _ops/origin/inventory/cqrd/okd.hosts -vvv _ops/origin/install.yml -e env=cqrd

# 节点管理
## 查看节点状态
oc get node -o wide
## 禁止节点参与调度
oc adm manage-node node3.oso.x --schedulable=false
## 节点容器迁移
oc adm manage-node node3.oso.x --evacuate --pod-selector="app=nginx"
## 删除节点
systemctl stop origin-node
systemctl disable origin-node
oc delete node node3.oso.x

# 标签管理
oc label node/master1.oso.x biz=oso-cluster

# router 管理
oc adm policy add-scc-to-user privileged -z router -n default
oc adm router router --replicas=1 --service-account=router -n default

# 部署 nginx
oc new-app --docker-image=dockerg.x/library/nginx:1.11.0 --insecure-registry=true --name=nginx11 -l app=nginx11 -n default
oc expose svc/nginx11 -n default
```

## 部分修复

```bash
# 安装基础包
ansible-playbook -i _ops/origin/inventory/oso.hosts -vvv _ops/origin/base/env_pkg.yml

# <Health Check>
ansible-playbook -i _ops/origin/inventory/oso.hosts -vvv playbooks/openshift-checks/pre-install.yml

# Node Bootstrap
ansible-playbook -i _ops/origin/inventory/oso.hosts -vvv playbooks/openshift-node/bootstrap.yml

# etcd Install
ansible-playbook -i _ops/origin/inventory/oso.hosts -vvv playbooks/openshift-etcd/config.yml

# NFS Install
ansible-playbook -i _ops/origin/inventory/oso.hosts -vvv playbooks/openshift-nfs/config.yml

# Load Balancer Install
ansible-playbook -i _ops/origin/inventory/oso.hosts -vvv playbooks/openshift-loadbalancer/config.yml

# Master Install
ansible-playbook -i _ops/origin/inventory/oso.hosts -vvv playbooks/openshift-master/config.yml

# Master Additional Install
ansible-playbook -i _ops/origin/inventory/oso.hosts -vvv playbooks/openshift-master/additional_config.yml

# <Node Join>
# https://docs.okd.io/latest/install_config/adding_hosts_to_existing_cluster.html
ansible-playbook -i _ops/origin/inventory/oso-add.hosts -vvv playbooks/openshift-node/scaleup.yml

# <Node Remove>
ansible-playbook -i _ops/origin/inventory/node-rm.hosts -vvv playbooks/adhoc/uninstall.yml

# <GlusterFS Install>
ansible-playbook -i _ops/origin/inventory/gfs.hosts -vvv playbooks/openshift-glusterfs/config.yml

# <GlusterFS Remove>
ansible-playbook -i _ops/origin/inventory/gfs.hosts -vvv playbooks/openshift-glusterfs/uninstall.yml
ansible-playbook -i _ops/origin/inventory/gfs.hosts -vvv -e "openshift_storage_glusterfs_wipe=true" playbooks/openshift-glusterfs/uninstall.yml

# Hosted Install
ansible-playbook -i _ops/origin/inventory/oso.hosts -vvv playbooks/openshift-hosted/config.yml

# Monitoring Install
ansible-playbook -i _ops/origin/inventory/oso.hosts -vvv playbooks/openshift-monitoring/config.yml

# <Web Console Install>
ansible-playbook -i _ops/origin/inventory/oso.hosts -vvv playbooks/openshift-web-console/config.yml

# Metrics Install
ansible-playbook -i _ops/origin/inventory/oso.hosts -vvv playbooks/openshift-metrics/config.yml

# <Logging Install>
ansible-playbook -i _ops/origin/inventory/oso.hosts -vvv playbooks/openshift-logging/config.yml

# <Prometheus Install>
ansible-playbook -i _ops/origin/inventory/oso.hosts -vvv playbooks/openshift-prometheus/config.yml

# Availability Monitoring Install
ansible-playbook -i _ops/origin/inventory/oso.hosts -vvv playbooks/openshift-monitor-availability/config.yml

# Service Catalog Install
ansible-playbook -i _ops/origin/inventory/oso.hosts -vvv playbooks/openshift-service-catalog/config.yml

# Management Install
ansible-playbook -i _ops/origin/inventory/oso.hosts -vvv playbooks/openshift-management/config.yml

# Descheduler Install
ansible-playbook -i _ops/origin/inventory/oso.hosts -vvv playbooks/openshift-descheduler/config.yml

# Node Problem Detector Install
ansible-playbook -i _ops/origin/inventory/oso.hosts -vvv playbooks/openshift-node-problem-detector/config.yml

# Autoheal Install
ansible-playbook -i _ops/origin/inventory/oso.hosts -vvv playbooks/openshift-autoheal/config.yml

# Operator Lifecycle Manager (OLM) Install (Technology Preview)
ansible-playbook -i _ops/origin/inventory/oso.hosts -vvv playbooks/olm/config.yml

# Upgrade: https://docs.okd.io/latest/upgrading/automated_upgrades.html
ansible-playbook -i _ops/origin/inventory/oso.hosts -vvv playbooks/byo/openshift-cluster/upgrades/v3_10/upgrade.yml
```

## 存储

- [glusterfs](config/gfs/README.md)
- [local volume](config/vol/README.md)
