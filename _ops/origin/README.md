# 本地虚拟机环境

okd | glusterfs
---|-----
v4.0 | vx.x gluster/gluster-centos:latest
v3.11 | v4.1 gluster/gluster-centos:gluster4u1_centos7
v3.9 | v3.12 gluster/gluster-centos:gluster3u12_centos7

## home

```bash
# 卸载存储
ansible-playbook -i _ops/origin/inventory/home/gfs.hosts -vvv \
    -e "openshift_storage_glusterfs_wipe=true" playbooks/openshift-glusterfs/uninstall.yml
# 卸载集群
ansible-playbook -i _ops/origin/inventory/home/okd.hosts -vvv _ops/origin/uninstall.yml

# 部署集群
ansible-playbook -i _ops/origin/inventory/home/okd.hosts -vvv _ops/origin/install.yml -e env=home
# 部署存储
ansible-playbook -i _ops/origin/inventory/home/gfs.hosts -vvv playbooks/openshift-glusterfs/config.yml
```

## cqrd

```bash
# 卸载存储
ansible-playbook -i _ops/origin/inventory/cqrd/gfs.hosts -vvv -e \
    "openshift_storage_glusterfs_wipe=true" playbooks/openshift-glusterfs/uninstall.yml
# 卸载集群
ansible-playbook -i _ops/origin/inventory/cqrd/okd.hosts -vvv _ops/origin/uninstall.yml

# 部署集群
ansible-playbook -i _ops/origin/inventory/cqrd/okd.hosts -vvv _ops/origin/install.yml -e env=cqrd
# 部署存储
ansible-playbook -i _ops/origin/inventory/cqrd/gfs.hosts -vvv playbooks/openshift-glusterfs/config.yml
```
