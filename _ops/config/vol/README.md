# Local volume

- [doc](https://docs.okd.io/latest/install_config/configuring_local.html)

![LVM](_images/LVM.png)

```bash
# 显示分区信息
fdisk -l
lsblk
blkid
cat /etc/fstab  # 系统启动时自动加载文件系统

# 分区管理(将磁盘切分)
fdisk /dev/sda
n/d/p
+2G
w
partprobe

# 物理卷管理
pvs
pvcreate /dev/sda3 /dev/sda4  # 将分区 /dev/sda3, /dev/sda4 格式化为物理卷
pvremove /dev/sda4

# (物理)卷组管理
vgs
vgcreate centos-data /dev/sda3 /dev/sda4
vgremove
vgreduce centos-data /dev/sda3
vgextend centos-data /dev/sda5
vgrename

# 逻辑卷管理(将卷组切分)
lvs
lvcreate -n docker -L 1G centos-data
## 快照卷
lvcreate --size 10 --snapshot --name docker-snapshot /dev/centos-data/docker
## 删除逻辑卷
umount /dev/centos-data/docker
lvremove /dev/centos-data/docker -y
## 扩容
lvextend -L12G /dev/centos-data/docker
lvextend -L +1G /dev/centos-data/docker
## 缩容
umount /data/docker
e2fsck -f /dev/centos-data/docker
resize2fs /dev/centos-data/docker 11G
lvreduce -L -1.992G /dev/centos-data/docker
resize2fs /dev/centos-data/docker
mount /dev/centos-data/docker /data/docker

# 挂载
mkfs.xfs /dev/centos-data/docker    # 创建文件系统
mount /dev/centos-data/docker /data/docker

# 擦除分区
wipefs -a /dev/sda3
```