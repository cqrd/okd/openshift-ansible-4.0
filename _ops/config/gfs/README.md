# GlusterFS

- `Brick`: 存储目录是 Glusterfs 的基本存储单元，由可信存储池中服务器上对外输出的目录表示。存储目录的格式由服务器和目录的绝对路径构成，具体表示为 SERVER:EXPORT. 例如：myhostname:/exports/myexportdir/
- `Volume`: 卷是存储目录的逻辑组合。大部分gluster管理操作是在卷上进行的。
- `Metadata`: 元数据关于数据的数据，用于描述文件、目录等的相关信息。
- `FUSE`: Filesystem in Userspace, 是一个内核模块，允许用户创建自己的文件系统无需修改内核代码。
- `DHT`: Distribute Hash Table
- `AFR`: Automatic File Replication
- `SAN`: Storage Area Network, 存储区域网络是一种高速网络或子网络，提供在计算机与存储之间的数据传输。
- `NAS`: Network-attached storage：网络附属存储是一种将分布、独立的数据整合为大型、集中化管理的数据中心，以便于对不同主机和应用服务器进行访问的技术

## 卷类型

### 分布式卷

![d](_images/distributed.png)

```bash
# 缺点: 不支持数据冗余，brick故障将导致数据完全丢失，并且必须依靠底层硬件来保护数据丢失
# 优点: 可以轻松便宜的缩放卷大小
gluster volume create test-volume \
    server1:/exp1 \
    server2:/exp2 \
    server3:/exp3
gluster volume info test-volume
```

### 副本卷

![d](_images/replicated.png)

```bash
# 优点: 数据副本可以根据需求保留在对应的brick中，即使一个brick故障，也可以从其复制的brick访问数据，提升了可靠性
# 缺点: 数据冗余
# 注意: replicas 和 brick 数量要一致
gluster volume create test-volume \
    replica 2 transport tcp \
    server1:/exp1 \
    server2:/exp2
```

### 分布式副本卷

![d](_images/distributed-replicated.png)

```bash
# 优点: 该卷用于需要由冗余和缩放引起的高可用性数据
# 缺点: 数据冗余
# 注意: brick 是 replicas 的 N 倍
gluster volume create test-volume \
    replica 2 transport tcp \
    server1:/exp1 \
    server2:/exp2 \
    server3:/exp3 \
    server4:/exp4
```

## 环境初始化

```bash
pvs
pvremove /dev/sdb
fdisk /dev/sdb
mkfs.xfs /dev/sdb1
mkfs.xfs -f /dev/sdb1
mkfs.xfs -f /dev/sdb2
vi /etc/fstab
'
/dev/sdb1 /data      xfs    defaults       0 0
'
mkdir /data
mount /data

# 推荐
mkfs.xfs -i size=30000 /dev/sdb2
mkdir -p /data/brick1
echo '/dev/sdb2 /data/brick1 xfs defaults 1 2' >> /etc/fstab
mount -a && mount
wipefs -a /dev/sdb2
```

## 基于 heketi

```bash
# 注意: secret 存在容器的环境变量 HEKETI_ADMIN_KEY 中
alias hk='heketi-cli --user admin --secret G0lHYVka/yFCsDZ9Gf05V+ug0KlpHWeYjhX7HB/qWw8='
hk volume create --size=1 --gid=10001 --name=oso-volume-001

mkdir -p /mnt/glusterfs/myVol1
mount -t glusterfs 10.221.150.85:/oso-volume-001 /mnt/glusterfs/myVol1
ls -lnZ /mnt/glusterfs/
'
drwxrwsr-x. 0 10001 system_u:object_r:fusefs_t:s0    myVol1
'

oc create -f svc-ep.yml
oc create -f pv.yml
oc create -f pvc.yml
oc create -f pod.yml

oc new-app --docker-image=dockerg.x/library/nginx:1.11.0 --insecure-registry=true --name=nginx11 -l app=nginx11 -n default
oc expose svc/nginx11 -n default
oc volume dc/nginx11 --add --name=nginx-storage -t pvc --claim-name=gluster-claim --overwrite

# @TODO 数据文件存储的路径和格式是什么?
# /var/lib/glusterd/vols/

# 查看数据卷定义
oc volumes dc/nginx11
```

## 基于 gluster

```bash
ansible oso -i hosts -u root -m shell -a "mkdir -p /data/brick1"
ansible oso -i hosts -u root -m shell -a "echo '/dev/sdb2 /data/brick1 xfs defaults 1 2' >> /etc/fstab"
ansible oso -i hosts -u root -m shell -a "mount -a && mount"

ansible oso -i hosts -u root -m shell -a "gluster peer probe 10.221.150.85"
ansible oso -i hosts -u root -m shell -a "gluster peer probe 10.221.150.86"
ansible oso -i hosts -u root -m shell -a "gluster peer probe 10.221.150.88"

ansible oso -i hosts -u root -m shell -a "mkdir -p /data/brick1/gv001"
gluster volume create gv001 replica 3 \
    10.221.150.85:/data/brick1/gv001 \
    10.221.150.86:/data/brick1/gv001 \
    10.221.150.88:/data/brick1/gv001
gluster volume start gv001
gluster volume info gv001

mount -t glusterfs 10.221.150.85:/gv001 /data/brick1/gv001

'
Mount failed. Please check the log file for more details.
'
tail /var/log/glusterfs/mnt-glusterfs-myVol1.log

'
volume create: gv001: failed: /data/brick1/gv001 is already part of a volume
'
ansible oso -i hosts -u root -m shell -a "rm -rf /data/brick1/gv001/.glusterfs"
ansible oso -i hosts -u root -m shell -a "setfattr -x trusted.glusterfs.volume-id /data/brick1/gv001/"
ansible oso -i hosts -u root -m shell -a "setfattr -x trusted.gfid /data/brick1/gv001/"
ansible oso -i hosts -u root -m shell -a "rm -rf /data/brick1"
```
